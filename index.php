<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Cocofest</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>

      <div class="nav_mobile main_nav">
          <div class="close_wrapper">         
             <a class="close-btn" href="#">
                <i class="pe-7s-close"></i>
              </a>
          </div>
         <div class="navbar-nav nav nav-fill w-100">
            <li class="nav-item active">
              <a class="nav-link" href="#about">TENTANG</br>COCOFEST</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#cara_ikutan">CARA</br>IKUTAN</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#aktivitas_bumn">AKTIVITAS</br>BUMN</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#gallery">GALLERY</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#tnc">SYARAT</br>& KETENTUAN</a>
            </li>
          </div>
      </div><!--END NAV MOBILE-->
      <header>
        <div class="logo_wrapper">
          <img src="images/logo_header.png">
        </div>
        <div class="container">
          <div class="nav_top">
            <ul class="nav justify-content-end">
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </li>
              <li class="nav-item navbar-light d-block d-lg-none">
                <button class="navbar-toggler nav-link" type="button">
                  <span class="navbar-toggler-icon"></span>
                </button>
              </li>
            </ul>
          </div>
          <div class="nav_bottom main_nav">
            <nav class="navbar navbar-expand-lg">
              <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="#about">TENTANG</br>COCOFEST</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#cara_ikutan">CARA</br>IKUTAN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#aktivitas_bumn">AKTIVITAS</br>BUMN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#gallery">GALLERY</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#tnc">SYARAT</br>& KETENTUAN</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </header>
      <section id="on_screen" class="on_screen main_wrapper">
        <div class="owl-carousel owl-top owl-theme">
            <div class="item">
              <div class="banner_wrapper" style="">
                <img src="images/banner1.jpg" class="img-fluid">
              </div>
            </div><!--END ITEM-->
            <div class="item">
              <div class="banner_wrapper" style="">
                <img src="images/banner2.jpg" class="img-fluid">
              </div>
            </div><!--END ITEM-->
            <div class="item">
              <div class="banner_wrapper" style="">
                <img src="images/banner3.jpg" class="img-fluid">
              </div>
            </div><!--END ITEM-->
        </div>
      </section>
      <section id="about" class="about h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-yellow">
            <div class="title_small">TENTANG</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content white-text text-center">
            <div class="white-text text_wrapper">
              <p>CO CO Fest merupakan ajang festival kreatif yang didukung oleh BUMN dengan tujuan untuk saling menghubungkan dan menginspirasi anak muda Indonesia untuk lebih kreatif dalam berkreasi yang
  bertepatan dengan Hari Sumpah Pemuda.</p>
              <p>
                Rangkaian acara dimulai dari aktivitas digital dengan puncak acara pada tanggal 28 Oktober 2018, yang terdiri dari Showcase &Workshop, Keynote Speech & Idea Talks, dan Meet &Greet dengan beberapa content creator terkemuka. Acara ini juga sekaligus sebagai ajang awarding night untuk para pemenang online competition.
                </p>
            </div>
            <div class="pengisi_acara_wrapper text-center">
              <div class="title">PENGISI ACARA :</div>
              <div class="pengisi_acara">
                <div class="thumb_wrapper mr-3">
                  <div class="img_wrapper">
                    <img src="images/barasuara.jpg">
                  </div>
                  <div class="name">Bara</br>Suara</div>
                </div><!--END THUMB-->
                <div class="thumb_wrapper">
                  <div class="img_wrapper">
                    <img src="images/yurayunita.jpg">
                  </div>
                  <div class="name">Yura</br>Yunita</div>
                </div><!--END THUMB-->
              </div><!--END PENGISI ACARA-->
            </div><!--END PENGISI ACARA WRAPPER-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="cara_ikutan" class="cara_ikutan h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-yellow">
            <div class="title_small">CARA IKUTAN</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content white-text">
            <div class="row margin_bottom_row">
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0 offset-lg-1">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step1.png">
                  </div>
                  <div class="col-numbering">
                    1
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                    <span class="orange">UPLOAD KONTEN KREATIF</span> TERKAIT TEMA BUMN DALAM BENTUK FOTO, VIDEO, ANIMASI ATAU GAMBAR PADA PLATFORM INSTAGRAM
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-3 d-none d-lg-block">
              </div><!--END COL-->
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step2.png">
                  </div>
                  <div class="col-numbering">
                    2
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                   <span class="orange">BUAT CAPTION MENARIK</span> TENTANG KONTEN BUMN KAMU DENGAN MENGGUNAKAN HASHTAG <span class="orange">#BUMNNYATABUATKITA</span> DAN <span class="orange">#bumnhadiruntukkita</span>
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-1 d-none d-lg-block">
              </div><!--END COL-->
            </div><!--END ROW-->
            <div class="row">
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0 offset-lg-1">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step3.png">
                  </div>
                  <div class="col-numbering">
                    3
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                   PADA CAPTION JANGAN LUPA <span class="orange">TAG 3 AKUN INSTAGRAM BUMN</span> TERKAIT KATEGORI KARYA KAMU
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-3 d-none d-lg-block">
              </div><!--END COL-->
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step4.png">
                  </div>
                  <div class="instagram">
                  @cocofest</br>indonesia
                  </div>
                  <div class="col-numbering">
                    4
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                    <span class="orange">tag hasil karyamu</span> ke akun instagram <span class="orange">@cocofestindonesia</span> dan juga <span class="orange">3 orang teman kamu</span> untuk ikutan
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-1 d-none d-lg-block">
              </div><!--END COL-->
            </div><!--END ROW-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="aktivitas_bumn" class="aktivitas_bumn main_wrapper">
        <div class="container">
          <div class="main-title text-center border-blue">
            <div class="title_small">KARYA</div>
            <div class="title_big">BUMN</div>
          </div>
          <div class="content blue-text">
            <div class="content_top">
              <div class="row">
                <div class="col-lg col-karya">
                  <div class="row row_images_karya mt-2">
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/1.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/2.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                  <div class="karya_bottom_wrapper">
                    <div class="karya_name">
                      BALKONDES</br>
                      ( BALAI EKONOMI DESA )
                    </div>
                    <div class="karya_button">
                      <a href="#" class="btn btn-detail">Lihat Detail</a>
                    </div>
                  </div><!--END KARYA BOTTOM-->
                </div><!--END COL-->
                <div class="col-lg col-karya offset-lg-1">
                  <div class="row row_images_karya mt-2">
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/5.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/6.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                  <div class="karya_bottom_wrapper">
                    <div class="karya_name">
                      RUMAH SEMENTARA</br>
                      DI LOMBOK
                    </div>
                    <div class="karya_button">
                      <a href="#" class="btn btn-detail">Lihat Detail</a>
                    </div>
                  </div><!--END KARYA BOTTOM-->
                </div><!--END COL-->
              </div><!--END ROW-->
              <div class="row">
                <div class="col-lg col-karya">
                  <div class="row row_images_karya mt-2">
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/3.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/4.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                  <div class="karya_bottom_wrapper">
                    <div class="karya_name">
                      RKB</br>
                      ( RUMAH KREATIF BUMN )
                    </div>
                    <div class="karya_button">
                      <a href="#" class="btn btn-detail">Lihat Detail</a>
                    </div>
                  </div><!--END KARYA BOTTOM-->
                </div><!--END COL-->
                <div class="col-lg col-karya offset-lg-1">
                  <div class="row row_images_karya mt-2">
                    <div class="col-lg-6 col-6 col-sm-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/9.jpg" class="img-fluid">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                  <div class="karya_bottom_wrapper">
                    <div class="karya_name">
                      EKSPEDISI</br>
                      PAPUA TERANG
                    </div>
                    <div class="karya_button">
                      <a href="#" class="btn btn-detail">Lihat Detail</a>
                    </div>
                  </div><!--END KARYA BOTTOM-->
                </div><!--END COL-->
              </div><!--END ROW-->
            </div><!--END TOP-->
            <div class="content_middle">
              <div class="title border-blue">
                <div class="title_small">ASET</div>
                <div class="title_big">BUMN</div>
              </div>
              <div class="row row_content_middle no-gutters">
                <div class="col-lg-4 col-content-middle px-lg-1 px-2 col-md-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/aset1.jpg" class="img-fluid">
                  </div>
                  <div class="name_wrapper text-center">
                    JEMBATAN</br>HOLTEKAMP
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
                <div class="col-lg-4 col-content-middle px-lg-1 px-2 col-md-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/aset2.jpg" class="img-fluid">
                  </div>
                  <div class="name_wrapper text-center">
                    PESAWAT NURTANIO</br>PTDI
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
                <div class="col-lg-4 col-content-middle px-lg-1 px-2 col-md-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/aset3.jpg" class="img-fluid">
                  </div>
                  <div class="name_wrapper text-center">
                    KAPAL PELNI</br>BINAIYA
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
              </div><!--END ROW-->
            </div><!--END MIDDLE-->
            <div class="content_middle">
              <div class="title border-blue">
                <div class="title_small">PRODUK</div>
                <div class="title_big">BUMN</div>
              </div>
              <div class="row row_content_middle no-gutters">
                <div class="col-lg-3 col-content-middle px-lg-1 px-2 col-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/produk1.jpg" class="img-fluid w-100">
                  </div>
                  <div class="name_wrapper text-center">
                    JEMBATAN</br>HOLTEKAMP
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
                <div class="col-lg-3 col-content-middle px-lg-1 px-2 col-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/produk2.jpg" class="img-fluid w-100">
                  </div>
                  <div class="name_wrapper text-center">
                    JEMBATAN</br>HOLTEKAMP
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
                <div class="col-lg-3 col-content-middle px-lg-1 px-2 col-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/produk3.jpg" class="img-fluid w-100">
                  </div>
                  <div class="name_wrapper text-center">
                     JEMBATAN</br>HOLTEKAMP
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
                <div class="col-lg-3 col-content-middle px-lg-1 px-2 col-md-6 mb-lg-0 mb-4">
                  <div class="img_wrapper">
                    <img src="images/aktivitas/produk4.jpg" class="img-fluid w-100">
                  </div>
                  <div class="name_wrapper text-center">
                     JEMBATAN</br>HOLTEKAMP
                  </div>
                  <div class="karya_button text-center">
                    <a href="#" class="btn btn-detail">Lihat Detail</a>
                  </div>
                </div><!--END COL-->
              </div><!--END ROW-->
            </div><!--END MIDDLE-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="gallery" class="gallery h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-yellow">
            <div class="title_small">GALLERY</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content white-text">
            <div class="tab_wrapper">
              <ul class="nav nav-pills justify-content-center nav-justified" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-nature" role="tab" aria-controls="pills-nature" aria-selected="true">NATURE</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-transportasi" role="tab" aria-controls="pills-transportasi" aria-selected="false">TRANSPORTASI</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-infrastuktur" role="tab" aria-controls="pills-infrastuktur" aria-selected="false">INFRASTRUKTUR</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-lifestyle" role="tab" aria-controls="pills-lifestyle" aria-selected="false">LIFESTYLE</a>
                </li>
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-nature" role="tabpanel" aria-labelledby="pills-nature-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <a href="images/gallery_1.jpg" data-fancybox="gallery" data-caption="Kiriman dari @handysalam | 1.200 vote">
                          <div class="img_wrapper">
                            <img src="images/gallery_1.jpg" class="w-100">
                          </div>
                        </a>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <a href="
                          https://source.unsplash.com/ty4X72BSsXY/1279x853" data-fancybox="gallery" data-caption="Kiriman dari @handysalam | 1.200 vote">
                          <div class="img_wrapper">
                            <img src="images/gallery_1.jpg" class="w-100">
                          </div>
                        </a>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
                <div class="tab-pane fade" id="pills-transportasi" role="tabpanel" aria-labelledby="pills-transportasi-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
                <div class="tab-pane fade" id="pills-infrastuktur" role="tabpanel" aria-labelledby="pills-infrastuktur-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
                <div class="tab-pane fade" id="pills-lifestyle" role="tabpanel" aria-labelledby="pills-lifestyle-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
              </div>
            </div><!--END TAB WRAPPER-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="tnc" class="tnc h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-blue">
            <div class="title_small">SYARAT & KETENTUAN</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content blue-text">
            <div class="list_tnc_wrapper">
              <ol>
                <li>
                  <span class="numbering">1.</span>Pihak Penyelenggara berhak mendiskualifikasi peserta yang melanggar syarat dan ketentuan dalam program kompetisi konten kreatif terkait tema BUMN.
                </li>
                <li>
                  <span class="numbering">2.</span>Pihak Penyelenggara berhak untuk melakukan penambahan atau pengurangan syarat dan ketentuan yang berlaku tanpa melakukan pemberitahuan terlebih dahulu.
                </li>
                <li>
                  <span class="numbering">3.</span>Keikutsertaan dalam program promosi ini tidak dipungut biaya. Hati-hati penipuan yang mengatasnamakan Pihak Penyelenggara.
                </li>
                <li>
                  <span class="numbering">4.</span>Karya yang diunggah merupakan karya original atau bukan plagiarisme.
                </li>
                <li>
                  <span class="numbering">5.</span>Karya yang diunggah merupakan karya yang belum pernah memenangkan perlombaan lainnya.
                </li>
                <li>
                  <span class="numbering">6.</span>Informasi lebih lanjut mengenai syarat dan ketentuan ini dapat diajukan via message Instagram @cocofestindonesia.
                </li>
                <li>
                  <span class="numbering">7.</span>Hadiah tidak dapat di-uangkan atau ditukar dengan apapun.
                </li>
              </ol>
            </div>
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section class="powered_by pt-4 pb-5">
        <div class="container">
          <div class="title">POWERED BY</div>
          <div class="logo_powered_by_wrapper text-center">
            <div class="logo mr-3">
              <img src="images/logo_mandiri.png" class="img-fluid">
            </div>
            <div class="logo">
              <img src="images/logo_telkom.png" class="img-fluid">
            </div>
          </div>
        </div><!--END CONTAINER-->
      </section>
      <footer>
        <div class="copyright">
          <div class="container">
            <div class="content">
               © Copyright 2018 - Cocofest.com. All rights reserved.
            </div>
          </div>
        </div>
      </footer>
      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="js/popper.js"></script>
      <script type="text/javascript" src="js/bootstrap.js"></script>
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
      <script>
        $('.owl-top').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            singleItem:true,
            dots:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.nav_mobile .close-btn').on('click', function(){
            $('body').removeClass('show-nav-mobile');
        });
        $('header .navbar-toggler').on('click', function(){
            $('body').addClass('show-nav-mobile');
        });

        $(".main_nav .nav-link").on("click", function() {
          var target = $(this.hash);
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        });
        $('[data-fancybox="login"]').fancybox({
          clickOutside: "close",
        });
        $('[data-fancybox="gallery"]').fancybox({
          fitToView:true,
          protect:true,
          caption : function( instance, item ) {
              var caption = '<div class="caption_name">' + $(this).data('caption') || '';
              if ( item.type === 'image' ) {
                  caption = (caption.length ? caption + '</div>' : '') + '<div class="fancybox-vote"><a href="#" class="btn btn-main">Beri Vote</a></div>' ;
              }
              return caption;
          },
          beforeShow: function() {
              $(".fancybox-caption").addClass('none');
          },
          afterShow: function() {
             $(".fancybox-caption").wrapInner("<div class='inner'></div>");
              var imageWidth = $(".fancybox-slide--current .fancybox-content").width();
              var imageHeight = $(".fancybox-slide--current .fancybox-content").height();
              var position = $(".fancybox-slide--current .fancybox-content").position();
              var top = position.top;
              $(".fancybox-caption").css("top", top + imageHeight + 15);
              setTimeout($(".fancybox-caption").removeClass('none'), 200);
              if(imageWidth > $(".fancybox-caption .inner").width()){
                $(".fancybox-caption").css("width", imageWidth + 16);
                $(".fancybox-caption .inner").addClass("image_width");
              }
          }
        });
      </script>
    </body>
</html>
