<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Cocofest</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Template CSS Files -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
        <link rel="stylesheet" type="text/css" href="css/pe-icon-7-filled.css">
        <link rel="stylesheet" type="text/css" href="css/helper.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>

      <div class="nav_mobile main_nav">
          <div class="close_wrapper">         
             <a class="close-btn" href="#">
                <i class="pe-7s-close"></i>
              </a>
          </div>
         <div class="navbar-nav nav nav-fill w-100">
            <li class="nav-item active">
              <a class="nav-link" href="#about">TENTANG</br>COCOFEST</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#cara_ikutan">CARA</br>IKUTAN</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#aktivitas_bumn">AKTIVITAS</br>BUMN</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#gallery">GALLERY</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#tnc">SYARAT</br>& KETENTUAN</a>
            </li>
          </div>
      </div><!--END NAV MOBILE-->
      <header>
        <div class="logo_wrapper">
          <img src="images/logo_header.png">
        </div>
        <div class="container">
          <div class="nav_top">
            <ul class="nav justify-content-end">
              <li class="nav-item nav-login">
                <a class="nav-link" data-fancybox="login" data-animation-duration="500" data-src="#loginModal" data-modal="true" href="javascript:void(0);" id="loginButton">
                  <span class="d-block d-sm-none"><i class="fa fa-user-circle-o" aria-hidden="true"></i></span><span class="d-sm-block d-none">MASUK/BERGABUNG</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              </li>
              <li class="nav-item navbar-light d-block d-lg-none">
                <button class="navbar-toggler nav-link" type="button">
                  <span class="navbar-toggler-icon"></span>
                </button>
              </li>
            </ul>
          </div>
          <div class="nav_bottom main_nav">
            <nav class="navbar navbar-expand-lg">
              <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="#about">TENTANG</br>COCOFEST</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#cara_ikutan">CARA</br>IKUTAN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#aktivitas_bumn">AKTIVITAS</br>BUMN</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#gallery">GALLERY</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#tnc">SYARAT</br>& KETENTUAN</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </header>
      <section id="on_screen" class="on_screen h-min-100 main_wrapper">
        <div class="owl-carousel owl-top owl-theme">
            <div class="item">
              <div class="banner_wrapper" style="background-image:url('images/bg-banner1.jpg');">
                <div class="container">
                  <div class="row row_text">
                    <div class="col-7 offset-5 col-banner">
                      <div class="top pl-lg-5">
                        COCO</br>FEST</br>2018
                      </div>
                      <div class="middle pl-lg-5">
                        <div class="event_name">
                          Content Creation festival
                        </div>
                        <div class="event_tagline">
                          Ciptakan Konten Kreatif</br>
                          Bersama BUMN Indonesia!
                        </div>
                      </div>
                      <div class="bottom slogan pl-lg-5">
                        #BUMNnyataBuatKita
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END CONTAINER-->
              </div>
            </div><!--END ITEM-->
            <div class="item">
              <div class="banner_wrapper" style="background-image:url('images/bg-banner1.jpg');">
                <div class="container">
                  <div class="row row_text">
                    <div class="col-7 offset-5 col-banner">
                      <div class="top pl-lg-5">
                        COCO</br>FEST</br>2018
                      </div>
                      <div class="middle pl-lg-5">
                        <div class="event_name">
                          Content Creation festival
                        </div>
                        <div class="event_tagline">
                          Ciptakan Konten Kreatif</br>
                          Bersama BUMN Indonesia!
                        </div>
                      </div>
                      <div class="bottom slogan pl-lg-5">
                        #BUMNnyataBuatKita
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END CONTAINER-->
              </div>
            </div><!--END ITEM-->
        </div>
      </section>
      <section id="about" class="about h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-yellow">
            <div class="title_small">TENTANG</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content white-text text-center">
            <div class="white-text text_wrapper">
              <p>CO CO Fest merupakan ajang festival kreatif yang didukung oleh BUMN dengan tujuan untuk saling menghubungkan dan menginspirasi anak muda Indonesia untuk lebih kreatif dalam berkreasi yang
  bertepatan dengan Hari Sumpah Pemuda.</p>
              <p>
                Rangkaian acara dimulai dari aktivitas digital dengan puncak acara pada tanggal 28 Oktober 2018, yang terdiri dari Showcase &Workshop, Keynote Speech & Idea Talks, dan Meet &Greet dengan beberapa content creator terkemuka. Acara ini juga sekaligus sebagai ajang awarding night untuk para pemenang online competition.
                </p>
            </div>
            <div class="pengisi_acara_wrapper text-center">
              <div class="title">PENGISI ACARA :</div>
              <div class="pengisi_acara">
                <div class="thumb_wrapper mr-3">
                  <div class="img_wrapper">
                    <img src="images/barasuara.jpg">
                  </div>
                  <div class="name">Bara</br>Suara</div>
                </div><!--END THUMB-->
                <div class="thumb_wrapper">
                  <div class="img_wrapper">
                    <img src="images/yurayunita.jpg">
                  </div>
                  <div class="name">Yura</br>Yunita</div>
                </div><!--END THUMB-->
              </div><!--END PENGISI ACARA-->
            </div><!--END PENGISI ACARA WRAPPER-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="cara_ikutan" class="cara_ikutan h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-yellow">
            <div class="title_small">CARA IKUTAN</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content white-text">
            <div class="row margin_bottom_row">
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0 offset-lg-1">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step1.png">
                  </div>
                  <div class="col-numbering">
                    1
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                    UPLOAD KONTEN KREATIF TERKAIT TEMA BUMN DALAM BENTUK FOTO, VIDEO, ANIMASI ATAU GAMBAR PADA PLATFORM INSTAGRAM
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-3 d-none d-lg-block">
              </div><!--END COL-->
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step2.png">
                  </div>
                  <div class="col-numbering">
                    2
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                   BUAT CAPTION MENARIK TENTANG KONTEN BUMN KAMU DENGAN MENGGUNAKAN HASHTAG #BUMNNYATABUATKITA
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-1 d-none d-lg-block">
              </div><!--END COL-->
            </div><!--END ROW-->
            <div class="row">
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0 offset-lg-1">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step3.png">
                  </div>
                  <div class="col-numbering">
                    3
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                   PADA CAPTION JANGAN LUPA MENYEBUT 3 NAMA BUMN TERKAIT KATEGORI KARYA KAMU
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-3 d-none d-lg-block">
              </div><!--END COL-->
              <div class="col-lg col-md-6 col-sm-6 mb-5 mb-lg-0">
                <div class="top">
                  <div class="icon_wrapper">
                    <img src="images/icon/step4.png">
                  </div>
                  <div class="instagram">
                  @cocofest</br>indonesia
                  </div>
                  <div class="col-numbering">
                    4
                  </div><!--END COL-->
                </div><!--END TOP-->
                <div class="bottom">
                  <p>
                   kamu wajib tag hasil karyamu ke akun instagram @cocofestindonesia dan juga 3 orang teman kamu untuk ikutan
                  </p>
                </div><!--END BOTTOM-->
              </div><!--END COL-->
              <div class="col-lg-1 d-none d-lg-block">
              </div><!--END COL-->
            </div><!--END ROW-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="aktivitas_bumn" class="aktivitas_bumn main_wrapper">
        <div class="container">
          <div class="main-title text-center border-blue">
            <div class="title_small">AKTIVITAS</div>
            <div class="title_big">BUMN</div>
          </div>
          <div class="content blue-text">
            <div class="text_wrapper mb-5">
              <div class="headings_1 text-center mb-4">
                DEFINISI BUMN
              </div>
              <p class="text-center">
                Badan Usaha Milik Negara adalah badan usaha yang seluruh atau sebagian besar modalnya dimiliki oleh negara melalui penyertaan secara langsung yang berasal dari kekayaan negara. Badan usaha ini juga merupakan instrumen penting dalam menjalankan dan mengembangkan perekonomian nasional. BUMN menjadi aset substansial bagi negara karena penghasilan dari bisnis ini akan menjadi devisa negara yang dimanfaatkan untuk kepentingan seluruh masyarakat Indonesia.
              </p>
            </div><!--END TEXT WRAPPER-->
            <div class="text_wrapper mb-4 text_wrapper_bottom">
              <div class="headings_1 text-center mb-5 mb-lg-4">
                AKTIVITAS BUMN
              </div>
              <div class="row row_text mb-4">
                <div class="col-lg-5 col-images mb-lg-0 mb-4">
                  <div class="row no-gutters">
                    <div class="col-6 pr-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/1.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                    <div class="col-6 pl-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/2.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END COL-->
                <div class="col-lg-7">
                  <div class="headings_2 mb-2">
                    BALKONDES ( BALAI EKONOMI DESA )
                  </div>
                  <p>
                    Balkondes merupakan interconnected tourism system berbasis komunitas yang merupakan program kerjasama antara Kementerian BUMN dan perusahaan BUMN. Balkondes merupakan sarana pengembangan perekonomian masyarakat setempat melalui pengembangan kegiatan UMKM di lingkungan sekitar dengan konsep homestay yang dapat dipadukan dengan eksplorasi seni dan budaya lokal untuk menunjang pariwisata sekitar. Balkondes memadukan teknologi digital dengan keunikan suasana pedesaan dalam berbagai kegiatan budaya, kesenian, UMKM, pendidikan dan agro wisata.
                  </p>
                </div><!--END COL-->
              </div><!--END ROW-->
              <div class="row row_text mb-4 row_right text-lg-right">
                <div class="col-lg-5 col-images order-1 order-lg-2 mb-lg-0 mb-4">
                  <div class="row no-gutters">
                    <div class="col-6 pr-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/3.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                    <div class="col-6 pl-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/4.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END COL-->
                <div class="col-lg-7 order-2 order-lg-1">
                  <div class="headings_2 mb-2">
                   RKB ( RUMAH KREATIF BUMN )
                  </div>
                  <p>
                    Sebagai upaya pemberdayaan ekonomi kerakyatan, khususnya bagi para pelaku usaha mikro kecil dan menengah, Kementrian BUMN bersama perusahaan milik negara membangun Rumah Kreatif BUMN (disingkat RKB) sebagai rumah bersama untuk berkumpul, belajar dan membina para pelaku UKM menjadi UKM Indonesia yang berkualitas. Rumah Kreatif BUMN (RKB) merupakan wadah bagi langkah kolaborasi BUMN dalam membentuk Digital Economy Ecosystem melalui pembinaan bagi UKM untuk meningkatkan kapasitas dan kapabilitas UKM itu sendiri. Rumah Kreatif BUMN akan mendampingi dan mendorong para pelaku UKM dalam menjawab tantangan utama pengembangan usaha UKM dalam hal Peningkatan kompetensi, Peningkatkan Akses Pemasaran dan Kemudahkan akses Permodalan.
                  </p>
                </div><!--END COL-->
              </div><!--END ROW-->
              <div class="row row_text mb-4">
                <div class="col-lg-5 col-images mb-4 mb-lg-0">
                  <div class="row no-gutters">
                    <div class="col-6 pr-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/5.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                    <div class="col-6 pl-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/6.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END COL-->
                <div class="col-lg-7">
                  <div class="headings_2 mb-2">
                    RUMAH SEMENTARA DI LOMBOK
                  </div>
                  <p>
                    Kementrian BUMN menyerahkan bantuan berupa rumah ramah gempa kepada warga Desa Sembalun, Lombok Timur, rumah ini bisa digunakan sebagai rumah sementara hingga warga mendapatkan bantuan pembangunan rumah dari pemerintah. Untuk saat ini rumah ramah gempa ini diprioritaskan bagi warga yang rumahnya hancur, anak-anak, dan lansia. Tercatat 1316 rumah di Desa Sembalun, Bumbung, Lombok Timur rusak berat.
                  </p>
                </div><!--END COL-->
              </div><!--END ROW-->
              <div class="row row_text mb-4 row_right text-lg-right">
                <div class="col-lg-5 col-images order-1 order-lg-2 mb-4 mb-lg-0">
                  <div class="row no-gutters">
                    <div class="col-6 pr-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/7.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                    <div class="col-6 pl-2">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/8.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END COL-->
                <div class="col-lg-7 order-2 order-lg-1">
                  <div class="headings_2 mb-2">
                   ELEKTRIFIKASI NASIONAL
                  </div>
                  <p>
                    Dalam rangka mewujudkan program BUMN hadir untuk negeri dan percepatan program peningkatan rasio elektrifikasi, sebanyak 35 perusahaan BUMN bersinergi untuk membiayai penyambungan listrik bagi Rumah Tangga Tidak Mampu di sekitar Jawa Barat bagian selatan dan Banten. Dalam mewujudkan program tersebut, BUMN sepakat untuk bersinergi sebagai upaya untuk menjamin terlaksananya penyambungan listrik bagi masyarakat Tidak Mampu dengan biaya penyambungan yang didanai dari dana program BUMN Hadir Untuk Negeri. Dana ini bersifat sponsorship dan bukan berasal dari dana Corporate Social Responsibility (CSR).
                  </p>
                </div><!--END COL-->
              </div><!--END ROW-->
              <div class="row row_text mb-4">
                <div class="col-lg-5 col-images mb-4 mb-lg-0">
                  <div class="row no-gutters w-100">
                    <div class="col-6 pl-2 offset-lg-6">
                      <div class="img_wrapper">
                        <img src="images/aktivitas/9.jpg" class="img-responsive w-100">
                      </div>
                    </div><!--END COL-->
                  </div><!--END ROW-->
                </div><!--END COL-->
                <div class="col-lg-7">
                  <div class="headings_2 mb-2">
                    EKSPEDISI PAPUA TERANG
                  </div>
                  <p>
                    adalah program PT PLN (Persero) untuk Indonesia Terang Listrik 2019 yang bekerja sama dengan 5 universitas (UI, ITB, UGM, ITS, UNCEN), Lembaga Penerbangan dan Antariksa Nasional (LAPAN), TNI AD, dan kementerian ESDM serta kementerian BUMN. Ekspedisi ini berlangsung selama 2 bulan, berhasil melakukan survei 755 desa atau kampung.
                  </p>
                </div><!--END COL-->
              </div><!--END ROW-->
            </div><!--END TEXT WRAPPER-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="gallery" class="gallery h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-yellow">
            <div class="title_small">GALLERY</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content white-text">
            <div class="tab_wrapper">
              <ul class="nav nav-pills justify-content-center nav-justified" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-nature" role="tab" aria-controls="pills-nature" aria-selected="true">NATURE</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-transportasi" role="tab" aria-controls="pills-transportasi" aria-selected="false">TRANSPORTASI</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-infrastuktur" role="tab" aria-controls="pills-infrastuktur" aria-selected="false">INFRASTRUKTUR</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-lifestyle" role="tab" aria-controls="pills-lifestyle" aria-selected="false">LIFESTYLE</a>
                </li>
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-nature" role="tabpanel" aria-labelledby="pills-nature-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <a href="images/gallery_1.jpg" data-fancybox="gallery" data-caption="Caption #1">
                          <div class="img_wrapper">
                            <img src="images/gallery_1.jpg" class="w-100">
                          </div>
                          <div class="hover">
                            <div class="icon_wrapper">
                              <i class="fa fa-heart" aria-hidden="true"></i>
                            </div>
                            <div class="likers">1.500</div>
                          </div>
                          <div class="hover">
                            <div class="icon_wrapper">
                              <i class="fa fa-heart" aria-hidden="true"></i>
                            </div>
                            <div class="likers">1.500</div>
                          </div>
                        </a>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
                <div class="tab-pane fade" id="pills-transportasi" role="tabpanel" aria-labelledby="pills-transportasi-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
                <div class="tab-pane fade" id="pills-infrastuktur" role="tabpanel" aria-labelledby="pills-infrastuktur-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
                <div class="tab-pane fade" id="pills-lifestyle" role="tabpanel" aria-labelledby="pills-lifestyle-tab">
                  <div class="gallery_wrapper">
                    <div class="row no-gutters">
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                      <div class="col-lg col-gallery col-sm-4 col-6">
                        <div class="img_wrapper">
                          <img src="images/gallery_1.jpg" class="w-100">
                        </div>
                        <div class="hover">
                          <div class="icon_wrapper">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                          </div>
                          <div class="likers">1.500</div>
                        </div>
                      </div><!--END COL-->
                    </div><!--END ROW-->
                  </div><!--END GALLERY-->
                </div>
              </div>
            </div><!--END TAB WRAPPER-->
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section id="tnc" class="tnc h-min-100 main_wrapper">
        <div class="container">
          <div class="main-title text-center border-blue">
            <div class="title_small">SYARAT & KETENTUAN</div>
            <div class="title_big">COCOFEST 2018</div>
          </div>
          <div class="content blue-text">
            <div class="list_tnc_wrapper">
              <ol>
                <li>
                  <span class="numbering">1.</span>Pihak Penyelenggara berhak mendiskualifikasi peserta yang melanggar syarat dan ketentuan dalam program kompetisi konten kreatif terkait tema BUMN.
                </li>
                <li>
                  <span class="numbering">2.</span>Pihak Penyelenggara berhak untuk melakukan penambahan atau pengurangan syarat dan ketentuan yang berlaku tanpa melakukan pemberitahuan terlebih dahulu.
                </li>
                <li>
                  <span class="numbering">3.</span>Keikutsertaan dalam program promosi ini tidak dipungut biaya. Hati-hati penipuan yang mengatasnamakan Pihak Penyelenggara.
                </li>
                <li>
                  <span class="numbering">4.</span>Karya yang diunggah merupakan karya original atau bukan plagiarisme.
                </li>
                <li>
                  <span class="numbering">5.</span>Karya yang diunggah merupakan karya yang belum pernah memenangkan perlombaan lainnya.
                </li>
                <li>
                  <span class="numbering">6.</span>Informasi lebih lanjut mengenai syarat dan ketentuan ini dapat diajukan via message Instagram @cocofestindonesia.
                </li>
                <li>
                  <span class="numbering">7.</span>Hadiah tidak dapat di-uangkan atau ditukar dengan apapun.
                </li>
              </ol>
            </div>
          </div><!--END CONTENT-->
        </div><!--END CONTAINER-->
      </section>
      <section class="powered_by pt-4 pb-5">
        <div class="container">
          <div class="title">POWERED BY</div>
          <div class="logo_powered_by_wrapper text-center">
            <div class="logo mr-3">
              <img src="images/logo_mandiri.png">
            </div>
            <div class="logo">
              <img src="images/logo_telkom.png">
            </div>
          </div>
        </div><!--END CONTAINER-->
      </section>
      <footer>
        <div class="copyright">
          <div class="container">
            <div class="content">
               © Copyright 2018 - Cocofest.com. All rights reserved.
            </div>
          </div>
        </div>
      </footer>
      <div id="loginModal" class="modal_fancybox modal_login" class="p-5">
          <div class="main-title text-center border-yellow">
            <div class="title_small">BERGABUNG</div>
          </div>
          <div class="close-btn">
            <a href="javascript:void(0);" data-fancybox-close>&times;</a>
          </div>
          <div class="form_wrapper">
            <form>
              <div class="form-group">
                <input type="text" class="form-control" required>
                <label>Nama</label>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" required>
                <label>Email</label>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" required>
                <label>No. Telepon</label>
              </div>
              <div class="form-group">
                <input type="password" class="form-control" required>
                <label>Password</label>
              </div>
              <div class="mb-3 button_wrapper">
                <button type="submit" class="btn btn-main w-100">KIRIM</button>
              </div>
              <div class="form_text">
                Sudah punya akun? <a href="#">Masuk Disini</a>
              </div>
            </form>
          </div>
      </div>
      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="js/popper.js"></script>
      <script type="text/javascript" src="js/bootstrap.js"></script>
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
      <script>
        $('.owl-top').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            singleItem:true,
            dots:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.nav_mobile .close-btn').on('click', function(){
            $('body').removeClass('show-nav-mobile');
        });
        $('header .navbar-toggler').on('click', function(){
            $('body').addClass('show-nav-mobile');
        });

        $(".main_nav .nav-link").on("click", function() {
          var target = $(this.hash);
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        });
        $('[data-fancybox="login"]').fancybox({
          clickOutside: "close",
        });
      </script>
    </body>
</html>
